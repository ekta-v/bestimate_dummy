#-------------------------------------------------
#
# Project created by QtCreator 2019-07-10T19:57:02
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BestimateDummy
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    Database/dbcontroller.cpp \
    Database/dbquery.cpp \
    Login/login.cpp \
        main.cpp \
    welcomepage.cpp \
    SignUp/signup.cpp \
    DialogMenu/dialogmenu.cpp \
    NewProject/newproject.cpp


HEADERS += \
    Database/dbcontroller.h \
    Database/dbquery.h \
    Login/login.h \
    welcomepage.h \
    constant.h \
    SignUp/signup.h \
    DialogMenu/dialogmenu.h \
    NewProject/newproject.h



FORMS += \
    Login/login.ui \
    welcomepage.ui \
    SignUp/signup.ui \
    DialogMenu/dialogmenu.ui \
    NewProject/newproject.ui



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

