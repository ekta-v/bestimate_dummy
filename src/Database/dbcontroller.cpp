#include "dbcontroller.h"
#include "constant.h"
#include <QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlError>


Database::Database(QWidget *parent) : QMainWindow(parent)
{}

/**
 * @see : Connect
 * @brief:This function is used to perform the connectivity between the
 * application and sqllite database.
 *
 * @param Nothing
 *
 * @return QSqlDatabase object.
*/

QSqlDatabase Database::Connect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(DATABASE_CLIENT);
    db.setDatabaseName(DATABASE_NAME);
    db.open();
    return db;
}

QSqlDatabase Database::Connect(QString DBName)
{
    QSqlDatabase db = QSqlDatabase::database();
    return db;
}
/**
 * @see : InsertExecQuery
 * @brief:This method is used to execute the insert query in the database.
 *
 * @param queryString : Sql Query
 * @param QSqlDatabase db database connection object
 *
 * @return QVariant.
*/
QVariant Database::InsertExecQuery(QString queryString,QSqlDatabase db)
{
    db.open();
    QSqlQuery query(db);
    if(!query.exec(queryString))
    {
        qDebug() << "SqLite error:" << query.lastError().text();
    }
    else
    {
    db.commit();
    }
    return query.lastInsertId();
}

/**
 * @see : SelectExecQuery
 * @brief:This method is used to execute the select query in the database.
 *
 * @param queryString : Sql Query
 * @param QSqlDatabase db database connection object
 *
 * @return QSqlQuery query.
*/
QSqlQuery Database::SelectExecQuery(QString queryString,QSqlDatabase db)
{
    db.open();
    QSqlQuery query(db);
    if(!query.exec(queryString))
    {
        qDebug() << "SqLite error:" << query.lastError().text();
    }
    else
    {
   qDebug() <<"SQlite select successful";
    }
    return query;
}
/**
 * @brief This method is used to remove the connection
 * @see removeDatabase
 * @param dbName
 * @param db
 */
void Database::removeDatabase(QString dbName, QSqlDatabase db)
{
    db.removeDatabase(dbName);
}
/**
 * @see DeleteTable
 * @brief This method is used to delete the Database
 * from table
 * @param dbName
 * @param db
 */
void Database::DeleteTable(QString queryString,QSqlDatabase db)
{
    db.open();
    QSqlQuery query(db);
    query.exec(queryString);
}
/**
 * @see Database::SelectExecQueryModel
 * @brief This method is used to execute the query
 * and return the model of it.
 *
 * @param queryString
 * @param db
 * @return QSqlQueryModel
 */
QSqlQueryModel *Database::SelectExecQueryModel(QString queryString, QSqlDatabase db)
{
    QSqlQueryModel *model = new QSqlQueryModel;
    QSqlQuery query(db);

    query.prepare(queryString);
    if(!query.exec())
    {
        qDebug() << "SqLite error:" << query.lastError().text();
    }
    else
    {
        query.exec();
        model->setQuery(query);
    }


    return model;
}

