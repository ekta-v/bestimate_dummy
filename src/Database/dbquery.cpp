#include "Database/dbquery.h"
#include <QDebug>
#include <QSqlRecord>

Query::Query()
{}

/**
 * @see : CreateUserTable
 * @brief:This method is used to create user table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateUserTable(QSqlDatabase db)
{
   QString query ="CREATE TABLE IF NOT EXISTS `User_Information` ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `First_Name` TEXT NOT NULL, `Last_Name` TEXT NOT NULL, `Email_id` TEXT NOT NULL, `Phone` NUMERIC NOT NULL UNIQUE, `username` TEXT NOT NULL UNIQUE, `password` TEXT NOT NULL, `Rememebr_me` INTEGER DEFAULT 0, `create_at` NUMERIC NOT NULL, `update_at` NUMERIC NOT NULL, `Is_License_Purchased` INTEGER NOT NULL, `Is_Free_Trail` INTEGER NOT NULL, `App_Status` TEXT NOT NULL, `Offline_Count` INTEGER DEFAULT 0, `Online_Count` INTEGER DEFAULT 0, `Has_Connection` INTEGER DEFAULT 1 )";
   dbObject.InsertExecQuery(query,db);
   return true;
}


/**
 * @see : CreateAppLicenseTable
 * @brief:This method is used to create Application license table table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateAppLicenseTable(QSqlDatabase db)
{
    QString query = "CREATE TABLE IF NOT EXISTS `App_License` ( `License_ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `id` INTEGER, `License_Key` NUMERIC NOT NULL UNIQUE, `Date_Issued` NUMERIC NOT NULL, `License_Status` TEXT NOT NULL, `Expiry` NUMERIC NOT NULL, `Transaction_Ref_Id` NUMERIC NOT NULL UNIQUE, `Trnsaction_Status` TEXT NOT NULL, `Subscription_Amount` INTEGER NOT NULL, FOREIGN KEY(`id`) REFERENCES `User_Information`(`id`) )";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : CreateProjectTable
 * @brief:This method is used to create Project table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateProjectTable(QSqlDatabase db)
{
    QString query = "CREATE TABLE IF NOT EXISTS `Project_Information` ( `Project_Id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `Client_id` INTEGER NOT NULL, `Project_Name` TEXT NOT NULL, `Division_Id` INTEGER NOT NULL, `Project_Template_Id` INTEGER NOT NULL, `Project_Start_Date` NUMERIC NOT NULL, `Project_End_Date` NUMERIC NOT NULL, `Project_Estimated_Budget` REAL NOT NULL, `Project_Contingency` INTEGER NOT NULL, `Project_Overhead` TEXT NOT NULL, `Project_Profit` REAL, FOREIGN KEY(`Division_Id`) REFERENCES `Division_info`(`Division_Id`), FOREIGN KEY(`Project_Template_Id`) REFERENCES `Project_Template_Info`(`Project_Template_Id`), FOREIGN KEY(`Client_Id`) REFERENCES `Client_Information`(`Client_Id`))";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : CreateLocationTable
 * @brief:This method is used to create Location table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateLocationTable(QSqlDatabase db)
{
    QString query = "CREATE TABLE IF NOT EXISTS `Location_Information` ( `Client_Id` INTEGER NOT NULL, `Location_Address_1` TEXT NOT NULL, `Location_Address_2` TEXT, `City` TEXT NOT NULL, `Prov` TEXT NOT NULL, `Country` TEXT NOT NULL, `Postal_Code` NUMERIC NOT NULL, `Phone_Number` NUMERIC NOT NULL, `Fax` NUMERIC NOT NULL, `Lot_Plan` TEXT NOT NULL, `Gps_Location` TEXT NOT NULL ,FOREIGN KEY(`Client_Id`) REFERENCES `Client_Information`(`Client_Id`))";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : CreateClientInformationTable
 * @brief:This method is used to create Client Information table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateClientInformationTable(QSqlDatabase db)
{
    QString query = "CREATE TABLE IF NOT EXISTS `Client_Information` ( `Client_Id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `id` INTEGER NOT NULL, `Client_Company_Name` TEXT NOT NULL, `Client_Contact_Person` TEXT, `Client_Address` TEXT NOT NULL, `Client_City` TEXT NOT NULL, `Client_Prove` TEXT NOT NULL, `Client_Country` TEXT NOT NULL, `Client_City_PostalCode` INTEGER NOT NULL, `Client_Phone_Number` NUMERIC NOT NULL, `Client_Fax_Number` NUMERIC NOT NULL, `Client_Email` TEXT NOT NULL UNIQUE, FOREIGN KEY(`id`) REFERENCES `User_Information`(`id`) )";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : CreateDivisionInfoTable
 * @brief:This method is used to create Division Information table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateDivisionInfoTable(QSqlDatabase db)
{
    QString query = "CREATE TABLE IF NOT EXISTS `Division_info` ( `Division_Id` NUMERIC NOT NULL UNIQUE, `Division_Description` TEXT NOT NULL, `Sub_Division_Id` INTEGER NOT NULL, `Sub_Divsion_Description` TEXT NOT NULL, PRIMARY KEY(`Division_Id`,`Sub_Division_Id`) )";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : CreateDrawInfoTable
 * @brief:This method is used to create Draw information table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::CreateDrawInfoTable(QSqlDatabase db)
{
  QString query  = "CREATE TABLE IF NOT EXISTS `Draw_Info` ( `Draw_Id` INTEGER NOT NULL, `Loan-Company` TEXT NOT NULL, `Contact_Person` TEXT NOT NULL, `Phone_Number` NUMERIC NOT NULL, `Fax` NUMERIC NOT NULL UNIQUE, `Email_Id` TEXT NOT NULL UNIQUE, `Loan_Amount` INTEGER NOT NULL, PRIMARY KEY(`Draw_Id`) )";
  dbObject.InsertExecQuery(query,db);
  return true;
}


/**
 * @see : CreateInvoiceTable
 * @brief:This method is used to create Invoice information table
 * into the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
 bool Query::CreateInvoiceTable(QSqlDatabase db)
 {
     QString query  = "CREATE TABLE IF NOT EXISTS `Invoice_Information` ( `Invoice_Id` INTEGER NOT NULL, `Invoice_Description` TEXT NOT NULL, `Draw_Id` INTEGER NOT NULL, `Invoice_Date` NUMERIC NOT NULL, `Created_By` TEXT NOT NULL, PRIMARY KEY(`Invoice_Id`) )";
     dbObject.InsertExecQuery(query,db);
     return true;
 }

 /**
  * @see : CreateProjectSubDivisionTable
  * @brief:This method is used to create Sub Division Information table
  * into the database
  *
  * @param QSqlDatabase db database connection object
  *
  * @return bool.
 */
 bool Query::CreateProjectSubDivisionTable(QSqlDatabase db)
 {
     QString query  = "CREATE TABLE IF NOT EXISTS `Project_SubDivision_Info` ( `Project_Id` INTEGER NOT NULL, `Divsion_Id` INTEGER NOT NULL, `Sub_Division_Id` INTEGER NOT NULL, FOREIGN KEY(`Divsion_Id`) REFERENCES `Division_info`(`Division_Id`), FOREIGN KEY(`Project_Id`) REFERENCES `Project_Information`(`Project_Id`) )";
     dbObject.InsertExecQuery(query,db);
     return true;
 }

 /**
  * @see : CreateProjectTemplateTable
  * @brief:This method is used to create Project Template related info table
  * into the database
  *
  * @param QSqlDatabase db database connection object
  *
  * @return bool.
 */
 bool Query::CreateProjectTemplateTable(QSqlDatabase db)
 {
     QString query  = "CREATE TABLE IF NOT EXISTS `Project_Template_Info` ( `Project_Template_Id` INTEGER NOT NULL UNIQUE, `Project_Template_Description` TEXT NOT NULL )";
     dbObject.InsertExecQuery(query,db);
     return true;
 }
 bool Query::AddUserDataUser(QString username, QString password, QString remember_me)
 {
     QString query = "INSERT INTO Users (`User_Name`,`User_Password`,`Login_Attempt`) VALUES ('"+username+"','"+password+"','"+remember_me+"')";
     dbObject.InsertExecQuery(query,db);
     return true;
 }
/**
 * @see : AddUserData
 * @brief:This method is used to make an entry of user
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/

 bool Query::AddUserData( QString first_name,
                          QString last_name,
                          QString email_id,
                          QString phonenumber,
                          QString username,
                          QString password,
                          QString createDate,
                          QString updateDate,
                          QString islicensepurchased,
                          QString isFreeTrial,
                          QString AppStatus,
                          QSqlDatabase db)
{

     QString query = "INSERT INTO User_Information( `First_Name`,`Last_Name`,`Email_id`,`Phone`,`username`,`password`, `create_at`,`update_at`,`Is_License_Purchased`,`Is_Free_Trail`,`App_Status` )"
                             " VALUES('"+first_name+"','"+last_name+"','"+email_id+"','"+phonenumber+"','"+username+"','"+password+"','"+createDate+"' ,'"+updateDate+"','"+islicensepurchased+"','"+isFreeTrial+"','"+AppStatus+"')";

    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddProjectData
 * @brief:This method is used to make an entry of Projects
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddProjectData(QString clientid,
                    QString project_name,
                    QString divisionId,
                    QString projectTemplateId,
                    QString startDate,
                    QString endDate,
                    QString estimatedBudget,
                    QString contingency,
                    QString overhead,
                    QString profit,
                    QSqlDatabase db)
{
    QString query = "INSERT INTO Project_Information( `Client_id`,`Project_Name`,`Division_Id`,`Project_Template_Id`,`Project_Start_Date`,`Project_End_Date`, `Project_Estimated_Budget`,`Project_Contingency`,`Project_Overhead`,`Project_Profit`)"
                    " VALUES('"+clientid+"','"+project_name+"','"+divisionId+"','"+projectTemplateId+"','"+startDate+"','"+endDate+"','"+estimatedBudget+"','"+contingency+"' ,'"+overhead+"','"+profit+"')";
    dbObject.InsertExecQuery(query, db);
    return true;
}

/**
 * @see : AddAppLicenseData
 * @brief:This method is used to make an entry of License Key coresponding to users
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddAppLicenseData(QString userid,
                       QString licenseKey,
                       QString dateIssued,
                       QString licenseStatus,
                       QString dateExpired,
                       QString trnasctionRefId,
                       QString transactionStatus,
                       QString subscriptionAmount,
                       QSqlDatabase db)
{
    QString query = "INSERT INTO App_License( `id`,`License_Key`,`Date_Issued`,`License_Status`,`Expiry`,`Transaction_Ref_Id`, `Transaction_Status`,`Subscription_Amount`)"
                    " VALUES('"+userid+"','"+licenseKey+"','"+dateIssued+"','"+licenseStatus+"','"+dateExpired+"','"+trnasctionRefId+"','"+transactionStatus+"','"+subscriptionAmount+"')";
    dbObject.InsertExecQuery(query, db);
    return true;
}

/**
 * @see : AddDivisionInfo
 * @brief:This method is used to make an entry of Division Info
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddDivisionInfo(QString description,
                     QString subDivisionId,
                     QString subDivisionDescription,
                     QSqlDatabase db)
{
    QString query = "INSERT INTO Division_Info( `Division_Description`,`Sub_Division_Id`,`Sub_Division_Description`)"
                    " VALUES('"+description+"','"+subDivisionId+"','"+subDivisionDescription+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddClientInformation
 * @brief:This method is used to make an entry of Client Info
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddClientInformation(QString userid,
                          QString clientCompanyName,
                          QString clientContactPerson,
                          QString  clientAddress,
                          QString clientCity,
                          QString clientProve ,
                          QString clientCountry ,
                          QString clientCityPostal,
                          QString clientPhone,
                          QString clientFax,
                          QString clientEmail,
                          QSqlDatabase db)
{
    QString query = "INSERT INTO Client_Information( `id`,`Client_Company_Name`,`Client_Contact_Person`,`Client_Address`,`Client_City`,`Client_Prove`,`Client_Country`,`Client_City_PostalCode`,`Client_Phone_Number`,`Client_Fax_Number`,`Client_Email`)"
                    " VALUES('"+userid+"','"+clientCompanyName+"','"+clientContactPerson+"','"+clientAddress+"','"+clientCity+"','"+clientProve+"','"+clientCountry+"','"+clientCityPostal+"','"+clientPhone+"','"+clientFax+"','"+clientEmail+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddProjectSubDivision
 * @brief:This method is used to make an entry of project and division mapping
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddProjectSubDivision(QString projectId, QString divisionId, QString subDivisionId, QSqlDatabase db)
{
    QString query = "INSERT INTO Project_SubDivision_Info( `Project_Id`,`Division_Id`,`Sub_Division_Id`)"
                    " VALUES('"+projectId+"','"+divisionId+"','"+subDivisionId+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddProjectTemplateInfo
 * @brief:This method is used to make an entry of project template information
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddProjectTemplateInfo(QString templateId, QString tempDescription,QSqlDatabase db)
{
    QString query = "INSERT INTO Project_Template_Info( `Project_Template_Id`,`Prject_Template_Description`)"
                    " VALUES('"+templateId+"','"+tempDescription+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}


/**
 * @see : AddLocationInfo
 * @brief:This method is used to make an entry of Location information
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddLocationInfo(QString clientId,
                     QString address_1,
                     QString address_2,
                     QString city,
                     QString prov,
                     QString country,
                     QString postalcode,
                     QString phonenumber,
                     QString fax,
                     QString plan,
                     QString gps,
                     QSqlDatabase db)
{
    QString query = "INSERT INTO Location_Information( `Client_Id`,`Location_Address_1`,`Location_Address_2`,`City`,`Prov`,`Country`,`Postal_Code`,`Phone_Number`,`Fax`,`Lot_Plan`,`Gps_Location`)"
                    " VALUES('"+clientId+"','"+address_1+"','"+address_2+"','"+city+"','"+prov+"','"+country+"','"+postalcode+"','"+phonenumber+"','"+fax+"','"+plan+"','"+gps+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddDrwaInfo
 * @brief:This method is used to make an entry of Draw information
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddDrwaInfo(QString loanCompany, QString contactPerson, QString phoneNumber, QString fax, QString email, QString loanAmount,QSqlDatabase db)
{
    QString query = "INSERT INTO Draw_Info( `Loan-Company`,`Contact_Person`,`Phone_Number`,`Fax`,`Email_Id`,`Loan_AMount`)"
                    " VALUES('"+loanCompany+"','"+contactPerson+"','"+phoneNumber+"','"+fax+"','"+email+"','"+loanAmount+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}

/**
 * @see : AddInvoiceInfo
 * @brief:This method is used to make an entry of Invoice information
 * information into the database
 *
 * @param QString username
 * @param QString password
 * @param bool rememberMe
 * @param QSqlDatabase db database connection object
 *
 * @return bool.
*/
bool Query::AddInvoiceInfo(QString invoiceDescription, QString drawId, QString invoiceDate, QString createBy,QSqlDatabase db)
{
    QString query = "INSERT INTO Invoice_Information( `Invoice_Description`,`Draw_Id`,`Invioce_Date`,`Create_By`)"
                    " VALUES('"+invoiceDescription+"','"+drawId+"','"+invoiceDate+"','"+createBy+"')";
    dbObject.InsertExecQuery(query,db);
    return true;
}
/**
 * @see : GetUserData
 * @brief:This method is used to get the user information
 * from the database
 *
 * @param QString username
 * @param QString password
 * @param QSqlDatabase db database connection object
 *
 * @return QSqlQuery object.
*/
QSqlQuery Query::GetUserData(QString username, QString password,QSqlDatabase db)
{
    QSqlQuery  result;
    QString query ="SELECT * FROM User_Information WHERE username='"+username+"' AND password ='"+password+"'";
    result = dbObject.SelectExecQuery(query,db);
    return result;
}

/**
 * @see : GetDivisionInfo
 * @brief:This method is used to get the division information
 * from the database
 *
 * @param QString username
 * @param QString password
 * @param QSqlDatabase db database connection object
 *
 * @return QSqlQuery object.
*/
QSqlQuery Query::GetDivisionInfo(QSqlDatabase db)
{
    QSqlQuery  result;
    QString query ="SELECT Division_Description FROM Division_Info" ;
    result = dbObject.SelectExecQuery(query,db);
    return result;
}

/**
 * @see : GetProjectTemplateInfo
 * @brief:This method is used to get the Project Template information
 * from the database
 *
 * @param QString username
 * @param QString password
 * @param QSqlDatabase db database connection object
 *
 * @return QSqlQuery object.
*/
QSqlQuery Query::GetProjectTemplateInfo(QSqlDatabase db)
{
    QSqlQuery  result;
    QString query ="SELECT Project_Template_Description FROM Project_Template_Info" ;
    result = dbObject.SelectExecQuery(query,db);
    return result;
}
/**
 * @see : UpdateUser
 * @brief:This method is used to delete the previous entry and add the
 * updated information into the database
 *
 * @param QString ,QString ,QString ,QString , db
 *
 * @return Nothing.
*/
void Query::UpdateUser(QString userId,QString expDate,QString onCnt,QString offCnt,QString flacSubs, QSqlDatabase db)
{
    QString query = "UPDATE user SET online_count= '"+onCnt+"' , offline_count = '"+offCnt+"',flacUser = '"+flacSubs+"',expiration_date = '"+expDate+"'  WHERE user_id = "+userId;
    dbObject.InsertExecQuery(query,db);

//    AddUserData(username,password,rememberMe,userId,db);
}
/**
 * @see : UpdateUserProfile
 * @brief:This method is used to update the profile of
 * the user
 *
 * @param QString,QString , db
 *
 * @return Nothing.
*/
void Query::UpdateUserProfile(QString userId,QString cover, QSqlDatabase db)
{
    QString query = "UPDATE user SET profile_image = '"+cover+"' WHERE user_id = "+userId;
    dbObject.InsertExecQuery(query,db);
}
/**
 * @see : IsUserAuthenticated
 * @brief:This method is used to check that user is available or not
 *
 * @param QString username
 * @param QString password
 * @param QSqlDatabase db database connection object
 *
 * @return bool
*/

bool Query::IsUserAuthenticated(QString username, QString password,QSqlDatabase db)
{
    auto queryResult = GetUserData(username,password,db);

    if(queryResult.next())
        return true;
    else
        return false;
}

/**
 * @see : IsAnyRecordAvailable
 * @brief:This method is used to check that user is available or not
 *
 * @param QSqlDatabase db database connection object
 *
 * @return bool
*/

bool Query::IsAnyRecordAvailable(QSqlDatabase db)
{
    QString query = "SELECT * FROM user";

    auto result = dbObject.SelectExecQuery(query,db);

    if(result.next())
        return true;
    else
        return false;
}
/**
 * @see : UpdateConnectionStatus
 * @brief:This method is used to update the connection status
 * in the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return Nothing
*/

void Query::UpdateConnectionStatus(QString previous, QString updated, QSqlDatabase db)
{
    QString query = "UPDATE user SET has_connection= '"+updated+"' WHERE has_connection = "+previous;
    dbObject.InsertExecQuery(query,db);
}
/**
 * @see : UpdateApplicationStatus
 * @brief:This method is used to update the connection status
 * in the database
 *
 * @param QSqlDatabase db database connection object
 *
 * @return Nothing
*/
void Query::UpdateApplicationStatus(QString previous, QString updated, QSqlDatabase db)
{
    QString query = "UPDATE user SET app_on_status= '"+updated+"' WHERE app_on_status = "+previous;
    dbObject.InsertExecQuery(query,db);
}
/**
 * @see : DeleteUser
 * @brief:This method is used to delete the user from the database
 *
 * @param QString username
 * @param QString password
 * @param QSqlDatabase db database connection object
 *
 * @return bool
*/

bool Query::DeleteUser(QString username ,QString password,QSqlDatabase db)
{
    QString query = "DELETE FROM user WHERE username='"+username+"' AND password='"+password+"'";
    dbObject.InsertExecQuery(query,db);

    return true;
}
/**
 * @see : SubscriptionExpireDate
 * @brief:This method is used to get the user
 * subscription date
 *
 * @param QString
 * @param QSqlDatabase db database connection object
 *
 * @return QString
*/

QString Query::SubscriptionExpireDate(QString userId,QSqlDatabase db)
{
    QString query = "SELECT expiration_date FROM user WHERE user_id = "+userId;
    auto result = dbObject.SelectExecQuery(query,db);
    QString date;
    while(result.next())
    {
        auto record = result.record();
        date = record.value("expiration_date").toString();
    }
    return date;


}











