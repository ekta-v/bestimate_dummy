#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlQueryModel>

class Database : public QMainWindow
{
    Q_OBJECT
public:
    explicit Database(QWidget *parent = 0);
    QSqlDatabase Connect();
    QSqlDatabase Connect(QString DBName);
    QVariant InsertExecQuery(QString queryString,QSqlDatabase db);
    QSqlQuery SelectExecQuery(QString queryString,QSqlDatabase db);

    void removeDatabase(QString dbName, QSqlDatabase db);
    void DeleteTable(QString queryString,QSqlDatabase db);
    QSqlQueryModel *SelectExecQueryModel(QString queryString,QSqlDatabase);

signals:

public slots:

private:
    static bool isConnect;
    QSqlDatabase dbConnect;
};

#endif // DB_CONTROLLER_H
