#ifndef DBQUERY_H
#define DBQUERY_H


#include <QObject>
#include <Database/dbcontroller.h>

struct PlaylistInfo;
struct ChartsInfo;

class Query
{
public:
    Query();
    //user
    bool CreateUserTable(QSqlDatabase);
    bool CreateAppLicenseTable(QSqlDatabase);
    bool CreateProjectTable(QSqlDatabase);
    bool CreateClientInformationTable(QSqlDatabase);
    bool CreateLocationTable(QSqlDatabase);
    bool CreateDivisionInfoTable(QSqlDatabase);
    bool CreateInvoiceTable(QSqlDatabase);
    bool CreateProjectSubDivisionTable(QSqlDatabase);
    bool CreateProjectTemplateTable(QSqlDatabase);
    bool CreateDrawInfoTable(QSqlDatabase);
    bool AddUserDataUser(QString, QString,QString);
    bool AddUserData( QString first_name, QString last_name,QString email_id, QString phonenumber, QString username, QString password, QString createDate, QString updateDate, QString islicensepurchased, QString isFreeTrial, QString AppStatus, QSqlDatabase db);
    bool AddProjectData(QString clientid, QString project_name, QString divisionId, QString projectTemplateId, QString startDate, QString endDate, QString estimatedBudget, QString contingency, QString overhead, QString profit,QSqlDatabase);
    bool AddDivisionInfo(QString description, QString subDivisionId, QString subDivisionDescription,QSqlDatabase);
    bool AddAppLicenseData(QString userid, QString licenseKey, QString dateIssued, QString licenseStatus, QString dateExpired, QString trnasctionRefId, QString transactionStatus, QString subscriptionAmount,QSqlDatabase);
    bool AddClientInformation(QString userid, QString clientCompanyName, QString clientContactPerson, QString  clientAddress, QString clientCity, QString clientProve , QString clientCountry , QString clientCityPostal, QString clientPhone, QString clientFax, QString clientEmail,QSqlDatabase);
    bool AddProjectSubDivision(QString projectId, QString divisionId, QString subDivisionId,QSqlDatabase);
    bool AddProjectTemplateInfo(QString templateId, QString tempDescription,QSqlDatabase);
    bool AddLocationInfo(QString clientId, QString address_1, QString address_2, QString city, QString prov, QString country, QString postalcode, QString phonenumber, QString fax, QString plan, QString gps,QSqlDatabase);
    bool AddDrwaInfo(QString loanCompany, QString contactPerson, QString phoneNumber, QString fax, QString email, QString loanAmount,QSqlDatabase);
    bool AddInvoiceInfo(QString invoiceDescription, QString drawId, QString invoiceDate, QString createBy,QSqlDatabase);
    void UpdateUser(QString, QString, QString, QString, QString, QSqlDatabase);
    QSqlQuery GetUserData(QString username,QString password,QSqlDatabase);
    QSqlQuery GetDivisionInfo(QSqlDatabase);
    QSqlQuery GetProjectTemplateInfo(QSqlDatabase);
    void UpdateUserProfile(QString userId, QString cover, QSqlDatabase db);
    bool IsUserAuthenticated(QString username, QString password,QSqlDatabase);
    bool IsAnyRecordAvailable(QSqlDatabase);
    void UpdateConnectionStatus(QString, QString, QSqlDatabase db);
    void UpdateApplicationStatus(QString, QString, QSqlDatabase db);
    bool DeleteUser(QString username, QString password,QSqlDatabase);
    QString SubscriptionExpireDate(QString userId, QSqlDatabase db);
    bool CreateSearchRecord(QSqlDatabase);
    QString GetCurrentRowOrder(QString songId, QString playlistId, QSqlDatabase db);

private:

    Database dbObject;
    QSqlDatabase db;
    QVector<QString> m_playlistId;
    QVector<QString> m_songs;
};

#endif // DBQUERY_H
