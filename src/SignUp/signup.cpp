#include "SignUp/signup.h"
#include "Database/dbquery.h"
#include "ui_signup.h"
#include <QPixmap>
#include <QWidget>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>
#include "welcomepage.h"
#include <QDateTime>


SignUp::SignUp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SignUp)
{
    QSqlDatabase db =  dbObject.Connect();
    if(db.isOpen())
    {
        qDebug() << "Database connected" << db;
    }
}

SignUp::~SignUp()
{
    delete ui;
}

QFrame* SignUp::AddLayout()
{
    QFrame* frameOuter = new QFrame;

        auto FrameInner = AddInnerFrame();

        QHBoxLayout* outerLayout = new QHBoxLayout;
        outerLayout->setContentsMargins( 400, 150, 400, 150);
        outerLayout->addWidget(FrameInner);
        frameOuter->setLayout(outerLayout);
        frameOuter->setStyleSheet("top:100px;"
                                "background:#000;"
                                "background-image: url(:/img/img/bg_banner.png)0 0 0 0 stretch stretch;"
                                "color:#90c441;"
                                "border:0px;"
                                );


    return frameOuter;
}

/**
 * @see : AddInnerFrame
 * @brief:This method is used to add all the widget
 * into one single frame for user Login-In
 *
 * @param Nothing
 *
 * @return Inner frame.
*/

QFrame* SignUp::AddInnerFrame()
{
    QFrame* frameInner = new QFrame;
    QVBoxLayout* layout = new QVBoxLayout();

    auto Image          =   AddLabelImg();
    auto HeadLabel      =   AddLabel();
    auto labelContent   =   AddLabelContent();
    auto username       =   AddUsername();
    auto password       =   AddPassword();
    auto radioButtonLicense          =   AddLicenseRadioBtn();
    auto buttonsign     =   AddDataBtn();

    auto firstname      =   AddFirstName();
    auto lastname       =   AddLastName();
    auto phone          =   AddPhone();
    auto email          =   AddEmail();
       layout->addWidget(Image);
       layout->addWidget(HeadLabel);
       layout->addWidget(labelContent);
       layout->addWidget(username);
       layout->addWidget(password);
       layout->addWidget(firstname);
       layout->addWidget(lastname);
       layout->addWidget(phone);
       layout->addWidget(email);
       layout->addWidget(radioButtonLicense);
       layout->addWidget(buttonsign);

       frameInner->setLayout(layout);
       frameInner->setStyleSheet("background :#000;"
                                 "border:1px solid #363636;"
                                );
       return frameInner;

}

/**
 * @see : AddLabelImg
 * @brief:This method is used to add a header image in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return Qlabel.
*/

QLabel* SignUp::AddLabelImg()
{
    QLabel *headerImg = new QLabel(this);
    QPixmap pix(":/img/img/logo.png");
    headerImg->setPixmap(pix);
    headerImg->setMaximumWidth(1000);
    headerImg->setAlignment(Qt::AlignCenter);
    headerImg->setStyleSheet("padding:20px 15px;"
                             "border-bottom:1px solid #363636;"
                             "border-top:0px;"
                             "border-left:0px;"
                             "border-right:0px;"
                             "background:#000;"
                             "text-align = center;"
                             );
    return headerImg;
}

/**
 * @see : AddLabel
 * @brief:This method is used to add a Sign In label in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return QLabel.
*/

QLabel* SignUp::AddLabel()
{
    QLabel *signup = new QLabel("REGISTRATION");
    signup->setStyleSheet("font-size: 25px;"
                          "color: #525252;"
                          "margin-bottom: 15px;"
                          "border:0px;"
                          "background:#000;"
                          );
    return signup;
}

/**
 * @see : AddLabelContent
 * @brief:This method is used to add a content in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return QLabel.
*/

QLabel* SignUp::AddLabelContent()
{
    QLabel *text = new QLabel();
    text->setText("Join the Bestimate group and estimate your projects");
    text->setStyleSheet("font-size: 16px;"
                        "color: #929292;"
                        "margin-bottom: 15px;"
                        "border:0px;"
                        "background:#000;"
                        );
    return text;
}

/**
 * @see : AddUsername
 * @brief:This method is used to add user name input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddUsername()
{
    username = new QLineEdit();
    username->setPlaceholderText("Username");
    username->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    qDebug() <<"entering into username feild";
    return username;
}

/**
 * @see : AddPassword
 * @brief:This method is used to add a password input Feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddPassword()
{
    password = new QLineEdit();
    password->setPlaceholderText("Password");
    password->setEchoMode(QLineEdit::Password);
    password->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    return password;
}

/**
 * @see : AddFirstName
 * @brief:This method is used to add user first name input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddFirstName()
{
    firstname = new QLineEdit();
    firstname->setPlaceholderText("First Name");
    firstname->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    qDebug() <<"entering into first name feild";
    return firstname;
}

/**
 * @see : AddLastName
 * @brief:This method is used to add user first name input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddLastName()
{
    lastname = new QLineEdit();
    lastname->setPlaceholderText("Last Name");
    lastname->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    qDebug() <<"entering into last name feild";
    return lastname;
}

/**
 * @see : AddPhone
 * @brief:This method is used to add user first name input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddPhone()
{
    phone = new QLineEdit();
    phone->setPlaceholderText("Phone Number");
    phone->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    qDebug() <<"entering into phone number feild";
    return phone;
}

/**
 * @see : AddEmail
 * @brief:This method is used to add user Email ID input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* SignUp::AddEmail()
{
    email = new QLineEdit();
    email->setPlaceholderText("Email ID");
    email->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    qDebug() <<"entering into email feild";
    return email;
}

/**
 * @see : AddLicenseRadioBtn
 * @brief:This method is used to add remember me in the
 * head of the layoutn
 *
 * @param Nothing
 *
 * @return QRadioButton.
*/

QRadioButton* SignUp::AddLicenseRadioBtn()
{
    radioButtonLicense = new QRadioButton();
    radioButtonLicense->setMaximumWidth(500);
    radioButtonLicense->setText("Do you wants to subscribe to bestimate");
    radioButtonLicense->setStyleSheet("font-size: 16px;"
                         "color: #929292;"
                         "border:0px;"
                         "padding: 13px 0px;"
                         "outline: none"
                         );
    return radioButtonLicense;
}
/**
 * @see : AddSubmitBtn
 * @brief:This method is used to add form submit button in
 * the head of the layout
 *
 * @param Nothing
 *
 * @return QPushButton.
*/

QPushButton* SignUp::AddDataBtn()
{
    QPushButton *buttonSubmit = new QPushButton();
    buttonSubmit->setText("Add-Submit");
    buttonSubmit->setMaximumWidth(150);
    buttonSubmit->setMaximumHeight(50);
    buttonSubmit->click();
    buttonSubmit->setStyleSheet("font-size: 21px;"
                          "text-align: center;"
                          "color: #fff;"
                          "background: #58c026;"
                          "border-radius: 4px;"
                          "border: none;"
                          );
    connect(buttonSubmit,SIGNAL(clicked()),this,SLOT(on_clickButton_clicked()));
    return buttonSubmit;
}


void SignUp::on_clickButton_clicked()
{

    qDebug() <<"Entered in onclick event";
    /**
     * QString createdDate = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");**/
    QString createdDate = "2020-06-22";
    QString userUsername = username->text();
    QString userPassword = password->text();
    QString UserFirstname = firstname->text();
    QString userLastname = lastname->text();
    QString usrPhone = phone->text();
    QString userEmail = email->text();
    bool licensePurchased = radioButtonLicense->isChecked();

    QString license = licensePurchased?"true":"false";
    QMessageBox* messageBox = new QMessageBox();

    messageBox->setWindowTitle("test");
    messageBox->setText("Welcome to Bestimate : " );
    messageBox->setIcon(QMessageBox::Information);
    messageBox->exec();
    if (sqlquery.IsUserAuthenticated(userUsername,userPassword,db))
    {
        messageBox->setText("User already exists : " + userUsername);
        messageBox->setIcon(QMessageBox::Information);
        messageBox->exec();
        if (messageBox->AcceptRole == 0 )
        {
        username->setText("");
        password->setText("");
        }
    }
    else
    {
        /**
        /** * @brief AddUserData
        /** * @return
        /** bool        AddUserData( first_name, last_name,email_id, phonenumber, username, password, createDate, updateDate,islicensepurchased, isFreeTrial, AppStatus, db);**/
       if(sqlquery.AddUserData(UserFirstname,userLastname,userEmail,usrPhone,userUsername,userPassword,createdDate,createdDate,license,"1","Free",db))

        {
            messageBox->setText("User created : " + userUsername);
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }
        else
        {
            messageBox->setText("Sorry data cannot be created");
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }
    }

}


