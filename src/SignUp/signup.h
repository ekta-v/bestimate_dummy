#ifndef SIGNUP_H
#define SIGNUP_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QWidget>
#include <QRadioButton>
#include "Database/dbcontroller.h"
#include "Database/dbquery.h"

namespace Ui {
class SignUp;
}

class SignUp : public QMainWindow
{
    Q_OBJECT
public:
    explicit SignUp(QWidget *parent = nullptr);
    ~SignUp();
    QFrame* AddLayout();

    void  Addwidget();
protected:
    QLabel* AddLabelImg();
    QLabel* AddLabelContent();
    QLabel* AddLabel();
    QLineEdit* AddUsername();
    QLineEdit* AddPassword();
    QLineEdit* AddFirstName();
    QLineEdit* AddLastName();
    QLineEdit* AddPhone();
    QLineEdit* AddEmail();

    QRadioButton* AddLicenseRadioBtn();
    QPushButton* AddDataBtn();
    QFrame* AddInnerFrame();

    QLineEdit *username;
    QLineEdit *password;
    QLineEdit *firstname;
    QLineEdit *lastname;
    QLineEdit *email;
    QLineEdit *phone;
    QRadioButton *radio;
    QRadioButton *radioButtonLicense;

public slots:
    void on_clickButton_clicked();
private:
    Ui::SignUp *ui;
    Database dbObject;
    QSqlDatabase db;
    Query sqlquery;
};

#endif // SIGNUP_H
