#include "dialogmenu.h"
#include "NewProject/newproject.h"
#include "welcomepage.h"
#include "ui_dialogmenu.h"

DialogMenu::DialogMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DialogMenu)
{
    ui->setupUi(this);
}

DialogMenu::~DialogMenu()
{
    delete ui;
}

void DialogMenu::on_pushButton_clicked()
{
    if(ui->radioButton->isChecked())
    {
        WelcomePage * wel = new WelcomePage();
        wel->showMaximized();
    }
    if(ui->radioButton_2->isChecked())
    {
        NewProject * np = new NewProject();
        np->showMaximized();
    }
}

void DialogMenu::on_pushButton_2_clicked()
{
    close();
}
