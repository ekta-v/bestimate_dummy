#ifndef DIALOGMENU_H
#define DIALOGMENU_H

#include <QWidget>

namespace Ui {
class DialogMenu;
}

class DialogMenu : public QWidget
{
    Q_OBJECT

public:
    explicit DialogMenu(QWidget *parent = 0);
    ~DialogMenu();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::DialogMenu *ui;
};

#endif // DIALOGMENU_H
