#include "Login/login.h"
#include "SignUp/signup.h"
#include "DialogMenu/dialogmenu.h"
#include "Database/dbquery.h"
#include "welcomepage.h"
#include "ui_login.h"
#include <QPixmap>
#include <QWidget>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>

Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{

    QSqlDatabase db =  dbObject.Connect();
    if(!db.isOpen())
    {
        qDebug() << "Database connected error" << db;

    }
        if (!sqlquery.CreateAppLicenseTable(db))
        {
            qDebug() <<"Error creating in App license table";
        }
        if(!sqlquery.CreateClientInformationTable(db))
        {
            qDebug() <<"Erro creating in Client Information Table";
        }
        if(!sqlquery.CreateDivisionInfoTable(db))
        {
           qDebug() <<"Error creating in Division Information Table";
        }
        if(!sqlquery.CreateDrawInfoTable(db))
        {
            qDebug() <<"Error creating in Draw Information table";
        }
        if(!sqlquery.CreateInvoiceTable(db))
        {
           qDebug() <<"Error creating in Invoice table";
        }
        if(!sqlquery.CreateLocationTable(db))
        {
           qDebug() <<"Error creating in Location table";
        }
        if(!sqlquery.CreateProjectSubDivisionTable(db))
        {
            qDebug() <<"Error creating in Project subdivsion table";
        }
        if(!sqlquery.CreateProjectTable(db))
        {
           qDebug() <<"Error creating in Project Information table";
        }
        if(!sqlquery.CreateProjectTemplateTable(db))
        {
           qDebug() <<"Error creating in Project Template Information table";
        }
        if(!sqlquery.CreateUserTable(db))
        {
            qDebug() <<"Error creating in User Table ";
        }
}

Login::~Login()
{
    delete ui;
}

/**
 * @see : AddLayout
 * @brief:This method is used to add the inner frame inside
 * the outer frame and create a complete layout.
 *
 * @param Nothing
 *
 * @return complete layout.
*/

QFrame* Login::AddLayout()
{
    QFrame* frameOuter = new QFrame;

        auto FrameInner = AddInnerFrame();

        QHBoxLayout* outerLayout = new QHBoxLayout;
        outerLayout->setContentsMargins( 400, 150, 400, 150);
        outerLayout->addWidget(FrameInner);
        frameOuter->setLayout(outerLayout);
        frameOuter->setStyleSheet("top:100px;"
                                "background:#000;"
                                "background-image: url(:/img/img/bg_banner.png)0 0 0 0 stretch stretch;"
                                "color:#90c441;"
                                "border:0px;"
                                );


    return frameOuter;
}

/**
 * @see : AddInnerFrame
 * @brief:This method is used to add all the widget
 * into one single frame for user Login-In
 *
 * @param Nothing
 *
 * @return Inner frame.
*/

QFrame* Login::AddInnerFrame()
{
    QFrame* frameInner = new QFrame;
    QVBoxLayout* layout = new QVBoxLayout();

    auto Image          =   AddLabelImg();
    auto HeadLabel      =   AddLabel();
    auto labelContent   =   AddLabelContent();
    auto username       =   AddUsername();
    auto password       =   AddPassword();
    auto radio          =   AddRadioBtn();
    auto button         =   AddSubmitBtn();
    auto signupbutton   =   AddSignUpBtn();

       layout->addWidget(Image);
       layout->addWidget(HeadLabel);
       layout->addWidget(labelContent);
       layout->addWidget(username);
       layout->addWidget(password);
       layout->addWidget(radio);
       layout->addWidget(button);
       layout->addWidget(signupbutton);
       frameInner->setLayout(layout);
       frameInner->setStyleSheet("background :#000;"
                                 "border:1px solid #363636;"
                                );
       return frameInner;

}

/**
 * @see : AddLabelImg
 * @brief:This method is used to add a header image in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return Qlabel.
*/

QLabel* Login::AddLabelImg()
{
    QLabel *headerImg = new QLabel(this);
    QPixmap pix(":/img/img/logo.png");
    headerImg->setPixmap(pix);
    headerImg->setMaximumWidth(1000);
    headerImg->setAlignment(Qt::AlignCenter);
    headerImg->setStyleSheet("padding:20px 15px;"
                             "border-bottom:1px solid #363636;"
                             "border-top:0px;"
                             "border-left:0px;"
                             "border-right:0px;"
                             "background:#000;"
                             "text-align = center;"
                             );
    return headerImg;
}

/**
 * @see : AddLabel
 * @brief:This method is used to add a Sign In label in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return QLabel.
*/

QLabel* Login::AddLabel()
{
    QLabel *signup = new QLabel("SIGN IN");
    signup->setStyleSheet("font-size: 25px;"
                          "color: #525252;"
                          "margin-bottom: 15px;"
                          "border:0px;"
                          "background:#000;"
                          );
    return signup;
}

/**
 * @see : AddLabelContent
 * @brief:This method is used to add a content in the
 * head of the layout
 *
 * @param Nothing
 *
 * @return QLabel.
*/

QLabel* Login::AddLabelContent()
{
    QLabel *text = new QLabel();
    text->setText("Join the Bestimate Platform ");
    text->setStyleSheet("font-size: 16px;"
                        "color: #929292;"
                        "margin-bottom: 15px;"
                        "border:0px;"
                        "background:#000;"
                        );
    return text;
}

/**
 * @see : AddUsername
 * @brief:This method is used to add user name input feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* Login::AddUsername()
{
    username = new QLineEdit();
    username->setPlaceholderText("Username");
    username->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    return username;
}

/**
 * @see : AddPassword
 * @brief:This method is used to add a password input Feild
 * in the head of the layout
 *
 * @param Nothing
 *
 * @return QLineEdit.
*/

QLineEdit* Login::AddPassword()
{
    password = new QLineEdit();
    password->setPlaceholderText("Password");
    password->setEchoMode(QLineEdit::Password);
    password->setStyleSheet("background: #363636;"
                            "border-radius: 4px;"
                            "border: none;"
                            "color: #fff;"
                            "font-size: 19px;"
                            "padding: 10px 15px;"
                            "width: 100%;"
                            "margin-bottom: 15px;"
                            );
    return password;
}

/**
 * @see : AddRadioBtn
 * @brief:This method is used to add remember me in the
 * head of the layoutn
 *
 * @param Nothing
 *
 * @return QRadioButton.
*/

QRadioButton* Login::AddRadioBtn()
{
    radio = new QRadioButton();
    radio->setMaximumWidth(125);
    radio->setText("Remember Me");
    radio->setStyleSheet("font-size: 16px;"
                         "color: #929292;"
                         "border:0px;"
                         "padding: 13px 0px;"
                         "outline: none"
                         );
    return radio;
}

/**
 * @see : AddSubmitBtn
 * @brief:This method is used to add form submit button in
 * the head of the layout
 *
 * @param Nothing
 *
 * @return QPushButton.
*/

QPushButton* Login::AddSubmitBtn()
{
    QPushButton *button = new QPushButton();
    button->setText("Sign In");
    button->setMaximumWidth(150);
    button->setMaximumHeight(50);
    button->click();
    button->setStyleSheet("font-size: 21px;"
                          "text-align: center;"
                          "color: #fff;"
                          "background: #58c026;"
                          "border-radius: 4px;"
                          "border: none;"
                          );
    connect(button,SIGNAL(clicked()),this,SLOT(on_loginButton_clicked()));
    return button;
}
void Login::on_loginButton_clicked()
{
    QString userUsername = username->text();
    QString userPassword = password->text();
    QMessageBox* messageBox = new QMessageBox();
    messageBox->setWindowTitle("test");
    if (sqlquery.IsUserAuthenticated(userUsername,userPassword,db))
    {
        messageBox->setText("Your Name is : " + userUsername);
        messageBox->setIcon(QMessageBox::Information);
        messageBox->exec();
        if (messageBox->AcceptRole == 0 )
        {
            DialogMenu *gm = new DialogMenu();
            gm->showMaximized();
    }
    }
    else
    {
        messageBox->setText("OOps no data found for " + userUsername);
        messageBox->setIcon(QMessageBox::Information);
        messageBox->exec();
    }
}

QPushButton* Login::AddSignUpBtn()
{
    QPushButton *button = new QPushButton();
    button->setText("Sign Up");
    button->setMaximumWidth(150);
    button->setMaximumHeight(50);
    button->click();
    button->setStyleSheet("font-size: 21px;"
                          "text-align: center;"
                          "color: #fff;"
                          "background: #58c026;"
                          "border-radius: 4px;"
                          "border: none;"
                          );
    connect(button,SIGNAL(clicked()),this,SLOT(on_signupButton_clicked()));
    return button;
}

void Login::on_signupButton_clicked()
{
    close();
    SignUp *sg = new SignUp();
    sg->AddLayout()->showMaximized();
}
