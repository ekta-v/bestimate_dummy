#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QRadioButton>
#include "Database/dbcontroller.h"
#include "Database/dbquery.h"

namespace Ui {
class Login;
}

class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = nullptr);
    ~Login();
    QFrame* AddLayout();

    void  Addwidget();

protected:
    QLabel* AddLabelImg();
    QLabel* AddLabelContent();
    QLabel* AddLabel();
    QLineEdit* AddUsername();
    QLineEdit* AddPassword();
    QRadioButton* AddRadioBtn();
    QPushButton* AddSubmitBtn();
    QPushButton* AddSignUpBtn();
    QFrame* AddInnerFrame();

    QLineEdit *username;
    QLineEdit *password;
    QRadioButton *radio;
private slots:
    void on_loginButton_clicked();
    void on_signupButton_clicked();
private:
    Ui::Login *ui;
    Database dbObject;
    QSqlDatabase db;
    Query sqlquery;
};

#endif // LOGIN_H
