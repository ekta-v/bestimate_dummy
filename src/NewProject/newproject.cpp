#include "newproject.h"
#include "ui_newproject.h"
#include "Database/dbquery.h"
#include <QMessageBox>
#include <QSqlQueryModel>
#include <QDebug>
#include <QException>

NewProject::NewProject(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewProject)
{
    ui->setupUi(this);
    QSqlDatabase db =  dbObject.Connect();
    if(!db.isOpen())
    {
        qDebug() << "Database connected error at line 15" << db;
    }
    QSqlQuery query = sqlquery.GetDivisionInfo(db);
    QSqlQueryModel *model = new QSqlQueryModel();
    if(!query.exec())
    {
        qDebug() <<"Could not get the data";
    }

    model->setQuery(query);
    ui->comboBox_DivisionID->setModel(model);
    ui->dateEdit_startDate->setCalendarPopup(true);
    ui->dateEdit_endDate->setCalendarPopup(true);
    ui->tabWidget->setTabText(0,"Project_Information");
    ui->tabWidget->setTabText(1,"Location_Information");
    ui->tabWidget->setTabText(2,"Client_Information");
    ui->tabWidget->setTabText(3,"Draw_Information");
    ui->tabWidget->setTabText(4,"Additional_Information");
    ui->lineEdit_clientADD->mask().Rectangle;
    QSqlQuery queryTemplate = sqlquery.GetProjectTemplateInfo(db);
    QSqlQueryModel *modelTemp = new QSqlQueryModel();
    if(!queryTemplate.exec())
    {
        qDebug() <<"Could not get the data";
    }

    modelTemp->setQuery(queryTemplate);
    ui->comboBox_TemplateID->setModel(modelTemp);

}

NewProject::~NewProject()
{
    delete ui;
}

void NewProject::on_pushButton_projectSave_clicked()
{


    QString clientID = "1";
    QString proName = ui->lineEdit_Projectname->text();
    QString divID = ui->comboBox_TemplateID->currentText();
    QString tempID = ui->comboBox_DivisionID->currentText();
    QDate startdate = ui->dateEdit_startDate->date();
    QDate end_date = ui->dateEdit_endDate->date();
    QString budget = ui->lineEdit_budget->text();
    QString cont = ui->lineEdit_cont->text();
    QString over = ui->lineEdit_overhead->text();
    QString profit = ui->lineEdit_profit->text();
    QMessageBox* messageBox = new QMessageBox();
        if(sqlquery.AddProjectData(clientID,proName,divID,tempID,startdate.toString(),end_date.toString(),budget,cont,over,profit,db))
        {
            messageBox->setText("User created : " + proName);
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }

}


void NewProject::on_pushButton_clientSave_clicked()
{
    QString userid = "1";
    QString clientcompanyname = ui->lineEdit_clientcompany->text();
    QString clientcontactperson =ui->lineEdit_clientcontactperson->text();
    QString clientycity = ui->lineEdit_clientCity->text();
    QString clientadd = ui->lineEdit_clientADD->text();
    QString clientphone = ui->lineEdit_clientPhone->text();
    QString clientfax = ui->lineEdit_clientFax->text();
    QString clientprove = ui->lineEdit_clientProv->text();
    QString clientemail  = ui->lineEdit_email->text();
    QString clientcountry = ui->lineEdit_clientCountry->text();
    QString clientpostal = ui->lineEdit_clientpostalcode->text();
    QMessageBox* messageBox = new QMessageBox();

    if(sqlquery.AddClientInformation(userid,clientcompanyname,clientcontactperson,clientadd,clientycity,clientprove,clientcountry,clientpostal,clientphone,clientfax,clientemail,db))
        {
            messageBox->setText("Company name created : " + clientcompanyname);
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }


}

void NewProject::on_pushButton_locationSave_clicked()
{
    QString clientid = "1";
    QString add1 =ui->lineEdit_locationAdd1->text();
    QString add2 = ui->lineEdit_locationAdd2->text();
    QString city = ui->lineEdit_locationCity->text();
    QString Prov = ui->lineEdit_locationProve->text();
    QString country = ui->lineEdit_locationCountry->text();
    QString postal = ui->lineEdit_locationPostal->text();
    QString phone = ui->lineEdit_locationPhone->text();
    QString fax = ui->lineEdit_locationFax->text();
    QString lot = ui->lineEdit_locationLot->text();
    QString gps = ui->lineEdit_locationGps->text();
    QMessageBox* messageBox = new QMessageBox();
    if(sqlquery.AddLocationInfo(clientid,add1,add2,city,Prov,country,postal,phone,fax,lot,gps,db))
        {
            messageBox->setText("Location created : " + add1);
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }
}

void NewProject::on_pushButton_drawSave_clicked()
{
    QString loancompay = ui->lineEdit_drawLoancompany->text();
    QString contactperson = ui->lineEdit_drawContactPerson->text();
    QString phone = ui->lineEdit_drawPhone->text();
    QString Fax = ui->lineEdit_drawFax->text();
    QString email = ui->lineEdit_drawEmail->text();
    QString loanAmount = ui->lineEdit_drawLoanAmount->text();
    QMessageBox* messageBox = new QMessageBox();
    if(sqlquery.AddDrwaInfo(loancompay,contactperson,phone,Fax,email,loanAmount,db))
        {
            messageBox->setText("Draw created : " + loancompay);
            messageBox->setIcon(QMessageBox::Information);
            messageBox->exec();
        }
}

void NewProject::on_pushButton_drawCancel_clicked()
{
    close();
}

void NewProject::on_pushButton_clientCancel_clicked()
{
    close();
}

void NewProject::on_pushButton_locationCancel_clicked()
{
    close();
}

void NewProject::on_pushButton_projectCancel_clicked()
{
    close();
}
