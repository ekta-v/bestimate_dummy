#ifndef NEWPROJECT_H
#define NEWPROJECT_H

#include <QWidget>
#include "Database/dbcontroller.h"
#include "Database/dbquery.h"
namespace Ui {
class NewProject;
}

class NewProject : public QWidget
{
    Q_OBJECT

public:
    explicit NewProject(QWidget *parent = 0);
    ~NewProject();

private slots:
    void on_pushButton_projectSave_clicked();
    void on_pushButton_clientSave_clicked();

    void on_pushButton_locationSave_clicked();

    void on_pushButton_drawSave_clicked();

    void on_pushButton_drawCancel_clicked();

    void on_pushButton_clientCancel_clicked();

    void on_pushButton_locationCancel_clicked();

    void on_pushButton_projectCancel_clicked();

private:
    Ui::NewProject *ui;
    Database dbObject;
    QSqlDatabase db;
    Query sqlquery;
};

#endif // NEWPROJECT_H
